# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-13 15:16+0000\n"
"PO-Revision-Date: 2020-09-14 18:57+0000\n"
"Last-Translator: dano6 <dano.kutka@gmail.com>\n"
"Language-Team: Slovak <https://translate.ubports.com/projects/ubports/"
"teleports/sk/>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:36
msgid "Choose a country"
msgstr "Zvoľte krajinu"

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr "Hľadať názov štátu..."

#: ../app/qml/components/DeleteDialog.qml:9
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""
"Správa bude zmazaná pre všetkých členov chatu. Naozaj chcete správu vymazať?"

#: ../app/qml/components/DeleteDialog.qml:10
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr "Správa bude zmazaná iba pre vás. Naozaj chcete správu vymazať?"

#: ../app/qml/components/DeleteDialog.qml:12
#: ../app/qml/delegates/MessageBubbleItem.qml:37
#: ../app/qml/pages/SettingsPage.qml:152 ../app/qml/pages/UserListPage.qml:104
#: ../app/qml/pages/UserListPage.qml:198
msgid "Delete"
msgstr "Vymazať"

#: ../app/qml/components/InputInfoBox.qml:91
msgid "Edit message"
msgstr "Editovať správu"

#: ../app/qml/components/MessageStatusRow.qml:36
msgid "Edited"
msgstr "Upravené"

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr "OK"

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:31
msgid "Cancel"
msgstr "Zrušiť"

#: ../app/qml/components/UserProfile.qml:72
msgid "Members: %1"
msgstr "Členov: %1"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called << %1 >> created"
msgstr "Vytvorený kanál s názvom <<%1>>"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called << %2 >>"
msgstr "%1 vytvoril skupinu s názvom  << %2 >>"

#: ../app/qml/delegates/MessageBubbleItem.qml:49
msgid "Copy"
msgstr "Kopírovať"

#: ../app/qml/delegates/MessageBubbleItem.qml:59
#: ../app/qml/pages/ChatInfoPage.qml:35
msgid "Edit"
msgstr "Upraviť"

#: ../app/qml/delegates/MessageBubbleItem.qml:67
msgid "Reply"
msgstr "Odpovedať"

#: ../app/qml/delegates/MessageBubbleItem.qml:73
msgid "Sticker Pack info"
msgstr "Informácie o balíčku nálepiek"

#: ../app/qml/delegates/MessageBubbleItem.qml:79
#: ../app/qml/pages/ChatListPage.qml:361
msgid "Forward"
msgstr "Preposlať"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:11
msgid "%1 joined the group"
msgstr "%1 sa pripojil(a) ku skupine"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:12
msgid "%1 added %2"
msgstr "%1 pridal(a) %2"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:35
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] "%1 užívateľ"
msgstr[1] "%1 užívatelia"
msgstr[2] "%1 užívateľov"

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr "%1 opustil(a) skupinu"

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
#: ../push/pushhelper.cpp:238
msgid "%1 removed %2"
msgstr "%1 odstránil(a) %2"

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr "Skupina bola povýšená na superskupinu"

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr "%1 sa pripojil(a) k Telegramu!"

#: ../app/qml/delegates/MessageContentBase.qml:37
msgid "Forwarded from %1"
msgstr "Preposlané od %1"

#. TRANSLATORS: This is the duration of a phone call in hours:minutes:seconds format
#: ../app/qml/delegates/MessageContentCall.qml:59
msgid "Duration: %1:%2:%3"
msgstr "Dĺžka hovoru: %1:%2:%3"

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr "Hlasová poznámka"

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr "%1 sa pripojil(a) pomocou pozývajúceho odkazu"

#: ../app/qml/delegates/MessagePinMessage.qml:5 ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr "%1 pripol správu"

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr "Snímok obrazovky bol vytvorený"

#: ../app/qml/delegates/MessageStickerItem.qml:20
msgid "Animated stickers not supported yet :("
msgstr "Animované nálepky zatiaľ nie sú podporované :("

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr "Chýba značka..."

#: ../app/qml/delegates/MessageUnsupported.qml:4
#: ../libs/qtdlib/messages/content/qtdmessageunsupported.cpp:12
msgid "Unsupported message"
msgstr "Nepodporovaná správa"

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr "Neznámy typ správy, pre podrobnosti pozrite logfile..."

#: ../app/qml/middleware/ChatMiddleware.qml:21
msgid "Are you sure you want to clear the history?"
msgstr "Naozaj chcete zmazať históriu?"

#: ../app/qml/middleware/ChatMiddleware.qml:22
#: ../app/qml/pages/ChatListPage.qml:131
msgid "Clear history"
msgstr "Vymazať históriu"

#: ../app/qml/middleware/ChatMiddleware.qml:31
msgid "Are you sure you want to leave this chat?"
msgstr "Naozaj chcete opustiť tento chat?"

#: ../app/qml/middleware/ChatMiddleware.qml:32
msgid "Leave"
msgstr "Opustiť"

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr "Zavrieť"

#: ../app/qml/pages/AboutPage.qml:13 ../app/qml/pages/ChatListPage.qml:83
msgid "About"
msgstr "O aplikácii"

#: ../app/qml/pages/AboutPage.qml:24 ../app/qml/pages/ChatInfoPage.qml:26
#: ../app/qml/pages/ConnectivityPage.qml:46
#: ../app/qml/pages/SecretChatKeyHashPage.qml:18
#: ../app/qml/pages/SettingsPage.qml:33 ../app/qml/pages/UserListPage.qml:29
msgid "Back"
msgstr "Späť"

#. TRANSLATORS: Application name.
#: ../app/qml/pages/AboutPage.qml:66 ../app/qml/pages/ChatListPage.qml:21
#: ../push/pushhelper.cpp:114 teleports.desktop.in.h:1
msgid "TELEports"
msgstr "TELEports"

#: ../app/qml/pages/AboutPage.qml:72
msgid "Version %1"
msgstr "Verzia %1"

#: ../app/qml/pages/AboutPage.qml:74
msgid " (git# %1)"
msgstr " (git# %1)"

#: ../app/qml/pages/AboutPage.qml:93
msgid "Get the source"
msgstr "Získať zdrojové kódy"

#: ../app/qml/pages/AboutPage.qml:94
msgid "Report issues"
msgstr "Nahlásiť problémy"

#: ../app/qml/pages/AboutPage.qml:95
msgid "Help translate"
msgstr "Pomôžte s prekladom"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Group Details"
msgstr "Informácie o skupine"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Profile"
msgstr "Profil"

#: ../app/qml/pages/ChatInfoPage.qml:41
msgid "Send message"
msgstr "Poslať správu"

#: ../app/qml/pages/ChatInfoPage.qml:61
msgid "Edit user data and press Save"
msgstr "Upraviť údaje o používateľovi a kliknúť na uložiť"

#: ../app/qml/pages/ChatInfoPage.qml:63 ../app/qml/pages/PreviewPage.qml:49
msgid "Save"
msgstr "Uložiť"

#: ../app/qml/pages/ChatInfoPage.qml:74 ../app/qml/pages/UserListPage.qml:59
msgid "Phone no"
msgstr "Telefónne číslo"

#: ../app/qml/pages/ChatInfoPage.qml:83 ../app/qml/pages/UserListPage.qml:67
msgid "First name"
msgstr "Meno"

#: ../app/qml/pages/ChatInfoPage.qml:92 ../app/qml/pages/UserListPage.qml:75
msgid "Last name"
msgstr "Priezvisko"

#: ../app/qml/pages/ChatInfoPage.qml:131
msgid "Notifications"
msgstr "Notifikácie"

#: ../app/qml/pages/ChatInfoPage.qml:160
msgid "%1 group in common"
msgid_plural "%1 groups in common"
msgstr[0] "%1 spoločná skupina"
msgstr[1] "%1 spoločné skupiny"
msgstr[2] "%1 spoločných skupín"

#: ../app/qml/pages/ChatInfoPage.qml:185
#: ../app/qml/pages/SecretChatKeyHashPage.qml:13
msgid "Encryption Key"
msgstr "Šifrovaný kľúč"

#: ../app/qml/pages/ChatListPage.qml:20
msgid "Select destination or cancel..."
msgstr "Vyberte cieľ alebo zrušte..."

#: ../app/qml/pages/ChatListPage.qml:37 ../app/qml/pages/ChatListPage.qml:64
#: ../app/qml/pages/SettingsPage.qml:22
msgid "Settings"
msgstr "Nastavenia"

#: ../app/qml/pages/ChatListPage.qml:54 ../libs/qtdlib/chat/qtdchat.cpp:85
msgid "Saved Messages"
msgstr "Uložené správy"

#: ../app/qml/pages/ChatListPage.qml:59 ../app/qml/pages/UserListPage.qml:18
msgid "Contacts"
msgstr "Kontakty"

#: ../app/qml/pages/ChatListPage.qml:70
msgid "Night mode"
msgstr "Nočný režim"

#: ../app/qml/pages/ChatListPage.qml:126
msgid "Leave chat"
msgstr "Opustiť chat"

#: ../app/qml/pages/ChatListPage.qml:142 ../app/qml/pages/UserListPage.qml:114
msgid "Info"
msgstr "Informácie"

#: ../app/qml/pages/ChatListPage.qml:359
msgid "Do you want to forward the selected messages to %1?"
msgstr "Prajete si vybrané správy preposlať 1%?"

#: ../app/qml/pages/ChatListPage.qml:374 ../app/qml/pages/ChatListPage.qml:398
msgid "Enter optional message..."
msgstr "Napíšte voliteľnú správu..."

#: ../app/qml/pages/ChatListPage.qml:382
msgid "Do you want to send the imported files to %1?"
msgstr "Prajete si stiahnuté súbory odoslať 1%?"

#: ../app/qml/pages/ChatListPage.qml:384
#: ../app/qml/pages/MessageListPage.qml:804
msgid "Send"
msgstr "Odoslať"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting"
msgstr "Pripojovanie"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:26
msgid "Offline"
msgstr "Nepripojený"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:30
#: ../libs/qtdlib/user/qtduserstatus.cpp:78
msgid "Online"
msgstr "Pripojený(á)"

#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Connecting To Proxy"
msgstr "Pripájanie k proxy"

#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Updating"
msgstr "Aktualizuje sa"

#: ../app/qml/pages/ConnectivityPage.qml:35
msgid "Connectivity"
msgstr "Pripojenie"

#: ../app/qml/pages/ConnectivityPage.qml:74
msgid "Telegram connectivity status:"
msgstr "Stav pripojenia Telegramu:"

#: ../app/qml/pages/ConnectivityPage.qml:81
msgid "Ubuntu Touch connectivity status:"
msgstr "Stav pripojenia Ubuntu Touch:"

#: ../app/qml/pages/ConnectivityPage.qml:88
msgid "Ubuntu Touch bandwith limited"
msgstr "Ubuntu Touch má obmedzený dátový tok"

#: ../app/qml/pages/ConnectivityPage.qml:88
msgid "Ubuntu Touch bandwith not limited"
msgstr "Ubuntu Touch má neobmedzený dátový tok"

#: ../app/qml/pages/MessageListPage.qml:32
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] "%1 člen"
msgstr[1] "%1 členovia"
msgstr[2] "%1 členov"

#: ../app/qml/pages/MessageListPage.qml:34
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] ", %1 pripojený(á)"
msgstr[1] ", %1 pripojení"
msgstr[2] ", %1 pripojení"

#: ../app/qml/pages/MessageListPage.qml:131
msgid "Telegram"
msgstr "Telegram"

#: ../app/qml/pages/MessageListPage.qml:334
msgid "You are not allowed to post in this channel"
msgstr "Nemáte oprávnenie k zasielaniu správ na tomto kanáli"

#: ../app/qml/pages/MessageListPage.qml:338
msgid "Waiting for other party to accept the secret chat..."
msgstr "Čaká sa na prijatie tajného chatu príjemcom..."

#: ../app/qml/pages/MessageListPage.qml:340
msgid "Secret chat has been closed"
msgstr "Tajný chat bol ukončený"

#: ../app/qml/pages/MessageListPage.qml:474
msgid "Type a message..."
msgstr "Napíšte správu..."

#: ../app/qml/pages/MessageListPage.qml:692
msgid "<<< Swipe to cancel"
msgstr "<<< Potiahnite pre zrušenie"

#: ../app/qml/pages/MessageListPage.qml:802
msgid "Do you want to share your location with %1?"
msgstr "Prajete si zdieľať vašu polohu s %1?"

#: ../app/qml/pages/MessageListPage.qml:815
msgid "Requesting location from OS..."
msgstr "Vyžadovaná poloha od operačného systému..."

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr "Výber obsahu"

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr "Súbor: "

#: ../app/qml/pages/SecretChatKeyHashPage.qml:65
msgid ""
"Check the image or the text. If they match with the ones on <b>%1</b>'s "
"device, end-to-end cryptography is granted."
msgstr ""
"Skontrolujte obrázok alebo text. Pokiaľ sa zhodujú s tými na zariadení <b>% "
"1 </b>, je spustená kryptografia typu end-to-end."

#: ../app/qml/pages/SettingsPage.qml:70 ../app/qml/pages/SettingsPage.qml:142
msgid "Logout"
msgstr "Odhlásiť"

#: ../app/qml/pages/SettingsPage.qml:86
msgid "Delete account"
msgstr "Zmazať účet"

#: ../app/qml/pages/SettingsPage.qml:102
msgid "Connectivity status"
msgstr "Stav pripojenia"

#: ../app/qml/pages/SettingsPage.qml:119
msgid "Toggle message status indicators"
msgstr "Zmeniť oznámenie o stave správy"

#: ../app/qml/pages/SettingsPage.qml:140
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""
"Varovanie: Odhlásenie zmaže všetky lokálne dáta z tohto zariadenia, vrátanie "
"súkromných chatov. Skutočne sa chcete odhlásiť?"

#: ../app/qml/pages/SettingsPage.qml:150
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"send using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""
"Varovanie: Súčasne so zmazaním účtu budú odstránené všetky dáta, ktoré ste "
"prijali alebo odoslali cez službu Telegram, okrem dát, ktoré sú špecificky "
"uložené mimo cloud Telegram. Naozaj chcete váš účet zmazať?"

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr "Pridať kontakt"

#: ../app/qml/pages/UserListPage.qml:47
msgid "The contact will be added. First and last name are optional"
msgstr "Kontakt bude pridaný. Meno a priezvisko sú voliteľné"

#: ../app/qml/pages/UserListPage.qml:49
msgid "Add"
msgstr "Pridať"

#: ../app/qml/pages/UserListPage.qml:119
msgid "Secret Chat"
msgstr "Tajný chat"

#: ../app/qml/pages/UserListPage.qml:196
msgid "The contact will be deleted. Are you sure?"
msgstr "Kontakt bude vymazaný. Ste si istý?"

#: ../app/qml/pages/WaitCodePage.qml:17
msgid "Enter Code"
msgstr "Zadajte kód"

#: ../app/qml/pages/WaitCodePage.qml:39
msgid "Code"
msgstr "Kód"

#: ../app/qml/pages/WaitCodePage.qml:54
msgid "We've send a code via telegram to your device. Please enter it here."
msgstr ""
"Na vaše zariadenie bol cez Telegram odoslaný kód. Zadajte ho prosím tu."

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr "Zadajte heslo"

#: ../app/qml/pages/WaitPasswordPage.qml:39
msgid "Password"
msgstr "Heslo"

#: ../app/qml/pages/WaitPasswordPage.qml:62
#: ../app/qml/pages/WaitPhoneNumberPage.qml:92
#: ../app/qml/pages/WaitRegistrationPage.qml:57
msgid "Next..."
msgstr "Ďalej..."

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr "Zadajte telefónne číslo"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:54
#: ../app/qml/pages/WaitPhoneNumberPage.qml:64
msgid "Phone number"
msgstr "Telefónne číslo"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:88
msgid "Please confirm your country code and enter your phone number."
msgstr "Potvrďte kód vášho štátu a zadajte Vaše telefónne číslo."

#: ../app/qml/pages/WaitRegistrationPage.qml:17
msgid "Enter your Name"
msgstr "Zadajte Vaše meno"

#: ../app/qml/pages/WaitRegistrationPage.qml:38
msgid "First Name"
msgstr "Meno"

#: ../app/qml/pages/WaitRegistrationPage.qml:44
msgid "Last Name"
msgstr "Priezvisko"

#: ../app/qml/stores/AuthStateStore.qml:63
msgid "Invalid phone number!"
msgstr "Neplatné telefónne číslo!"

#: ../app/qml/stores/AuthStateStore.qml:70
msgid "Invalid code!"
msgstr "Neplatný kód!"

#: ../app/qml/stores/AuthStateStore.qml:77
msgid "Invalid password!"
msgstr "Neplatné heslo!"

#: ../app/qml/stores/AuthStateStore.qml:97
msgid "Auth code not expected right now"
msgstr "Práve teraz nie je očakávaný overovací kód"

#: ../app/qml/stores/AuthStateStore.qml:103
msgid "Oops! Internal error."
msgstr "Uff! Zaznamenaná vnútorná chyba."

#: ../app/qml/stores/AuthStateStore.qml:122
msgid "Registration not expected right now"
msgstr "Registrácia sa neočakáva hneď teraz"

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "Error"
msgstr "Chyba"

#: ../app/qml/stores/ChatStateStore.qml:39
msgid "No valid location received after 180 seconds!"
msgstr "V časovom intervale 180 sekúnd nebola prijatá žiadna platná poloha!"

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr "Registrácia zasielania notifikácií zlyhala"

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr "Chýba Ubuntu One účet"

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr "Pre príjem oznámení sa pripojte k Ubuntu One účtu."

#: ../libs/qtdlib/chat/qtdchat.cpp:333
msgid "Draft:"
msgstr "Koncept:"

#: ../libs/qtdlib/chat/qtdchat.cpp:613
msgid "is choosing contact..."
msgstr "vyberá kontakt..."

#: ../libs/qtdlib/chat/qtdchat.cpp:614
msgid "are choosing contact..."
msgstr "vyberajú kontakt..."

#: ../libs/qtdlib/chat/qtdchat.cpp:617
msgid "is choosing location..."
msgstr "vyberá polohu..."

#: ../libs/qtdlib/chat/qtdchat.cpp:618
msgid "are choosing location..."
msgstr "vyberajú polohu..."

#: ../libs/qtdlib/chat/qtdchat.cpp:623
msgid "is recording..."
msgstr "nahráva..."

#: ../libs/qtdlib/chat/qtdchat.cpp:624
msgid "are recording..."
msgstr "nahrávajú..."

#: ../libs/qtdlib/chat/qtdchat.cpp:627
msgid "is typing..."
msgstr "píše..."

#: ../libs/qtdlib/chat/qtdchat.cpp:628
msgid "are typing..."
msgstr "píšu..."

#: ../libs/qtdlib/chat/qtdchat.cpp:631
msgid "is doing something..."
msgstr "niečo robí..."

#: ../libs/qtdlib/chat/qtdchat.cpp:632
msgid "are doing something..."
msgstr "niečo robia..."

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF"
msgstr "GIF"

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF,"
msgstr "GIF,"

#: ../libs/qtdlib/messages/content/qtdmessagebasicgroupchatcreate.cpp:25
#: ../libs/qtdlib/messages/content/qtdmessagesupergroupchatcreate.cpp:23
msgid "created this group"
msgstr "vytvoril túto skupinu"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:68
msgid "Call Declined"
msgstr "Hovor odmietnutý"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:71
msgid "Call Disconnected"
msgstr "Hovor prerušený"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:74
msgid "Call Ended"
msgstr "Hovor ukončený"

#. TRANSLATORS: This is a duration in hours:minutes:seconds format - only arrange the order, do not translate!
#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:78
msgid "%1:%2:%3"
msgstr "%1:%2:%3"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:79
msgid "Outgoing Call (%1)"
msgstr "Odchádzajúci hovor (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:80
msgid "Incoming Call (%1)"
msgstr "Prichádzajúci hovor (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Cancelled Call"
msgstr "Hovor zrušený"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Missed Call"
msgstr "Zmeškaný hovor"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:86
msgid "Call"
msgstr "Volať"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "added one or more members"
msgstr "pridaný jeden alebo viac členov"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "joined the group"
msgstr "sa pripojil k skupine"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangephoto.cpp:20
msgid "changed the chat photo"
msgstr "zmenil fotografiu skupiny"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangetitle.cpp:19
msgid "changed the chat title"
msgstr "zmenil názov skupiny"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "left the group"
msgstr "opustil skupinu"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "removed a member"
msgstr "odstránil člena"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletephoto.cpp:8
msgid "deleted the chat photo"
msgstr "odstránil fotografiu chatu"

#: ../libs/qtdlib/messages/content/qtdmessagechatjoinbylink.cpp:8
msgid "joined the group via the public link"
msgstr "sa pripojil ku skupine prostredníctvom verejného odkazu"

#: ../libs/qtdlib/messages/content/qtdmessagechatsetttl.cpp:19
msgid "message TTL has been changed"
msgstr "TTL správa zmenená"

#: ../libs/qtdlib/messages/content/qtdmessagechatupgradefrom.cpp:29
#: ../libs/qtdlib/messages/content/qtdmessagechatupgradeto.cpp:29
msgid "upgraded to supergroup"
msgstr "povýšená na supergroup"

#: ../libs/qtdlib/messages/content/qtdmessagecontact.cpp:18
msgid "Contact"
msgstr "Kontakt"

#: ../libs/qtdlib/messages/content/qtdmessagecontactregistered.cpp:9
msgid "has joined Telegram!"
msgstr "sa pripojil k Telegramu!"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:19
msgid "Today"
msgstr "Dnes"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:21
msgid "Yesterday"
msgstr "Včera"

#. TRANSLATORS: String in date separator label. For messages within a week: full weekday name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:24
msgid "dddd"
msgstr "dddd"

#. TRANSLATORS: String in date separator label. For messages of pas years: date number, month name and year
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:27
msgid "dd MMMM yyyy"
msgstr "dd MMMM rrrr"

#. TRANSLATORS: String in date separator label. For messages older that a week but within the current year: date number and month name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:30
msgid "dd MMMM"
msgstr "dd MMMM"

#: ../libs/qtdlib/messages/content/qtdmessagelocation.cpp:19
msgid "Location"
msgstr "Poloha"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo"
msgstr "Fotka"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo,"
msgstr "Fotka,"

#: ../libs/qtdlib/messages/content/qtdmessagepinmessage.cpp:19
msgid "Pinned Message"
msgstr "Pripnutá správa"

#: ../libs/qtdlib/messages/content/qtdmessagesticker.cpp:20
msgid "Sticker"
msgstr "Nálepka"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video"
msgstr "Video"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video,"
msgstr "Video,"

#: ../libs/qtdlib/messages/content/qtdmessagevideonote.cpp:30
msgid "Video message"
msgstr "Video správa"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message"
msgstr "Hlasová správa"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message,"
msgstr "Hlasová správa,"

#: ../libs/qtdlib/messages/qtdmessage.cpp:84
msgid "Me"
msgstr "Ja"

#: ../libs/qtdlib/messages/qtdmessagecontentfactory.cpp:133
msgid "Unimplemented:"
msgstr "Neimplementované:"

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:252
msgid "Unread Messages"
msgstr "Neprečítané správy"

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr "Naposledy videné pred mesiacom"

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr "Naposledy videné pred týždňom"

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr "Naposledy videné "

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr "dd.MM.yy hh:mm"

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr "Videné nedávno"

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr "vám poslal správu"

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr "Vám poslal(a) fotografiu"

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr "Vám poslal(a) sticker"

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr "Vám poslal(a) video"

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr "Vám poslal(a) dokument"

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr "Vám poslal(a) audio správu"

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr "Vám poslal(a) hlasovú správu"

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr "zdieľal(a) s vami kontakt"

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr "Vám zaslal(a) mapu"

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr "%1: %2"

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr "%1 poslal(a) skupine správu"

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr "%1 poslal(a) skupine fotografiu"

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr "%1 poslal(a) skupine sticker"

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr "%1 poslal(a) skupine video"

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr "%1 poslal(a) skupine dokument"

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr "%1 poslal(a) skupine hlasovú správu"

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr "%1 poslal(a) skupine GIF"

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr "%1 poslal(a) skupine kontakt"

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr "%1poslal(a) skupine mapu"

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr "%1 Vás pozval(a) do skupiny"

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr "%1 zmenil(a) názov skupiny"

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr "%1 zmenil(a) fotografiu skupiny"

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr "%1 pozval(a) %2"

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr "%1 Vás odstránil(a) zo skupiny"

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr "%1 opustil(a) skupinu"

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr "%1 sa vrátil do skupiny"

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr "@ %1"

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr "%1 sa registroval"

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr "%1 sa pripojil k Telegramu!"

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr "Nové prihlásenie z neznámeho zariadenia"

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr "%1 @ %2"

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr "profilová fotografia aktualizovaná"

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr "Máte novú správu"

#~ msgid "not available"
#~ msgstr "nie je dostupné"

#~ msgid "Archived chats"
#~ msgstr "Archivované čety"

#~ msgid "MMMM dd, yyyy"
#~ msgstr "MMMM dd, yyyy"

#~ msgid "MMMM dd"
#~ msgstr "MMMM dd"

#~ msgid "Incorrect auth code length."
#~ msgstr "Nesprávna dĺžka overovacieho kódu."

#~ msgid "Phone call"
#~ msgstr "Hovor"

#~ msgid "sent an audio message"
#~ msgstr "zaslaná audio správa"

#~ msgid "sent a photo"
#~ msgstr "zaslaná fotografia"

#~ msgid "sent a video"
#~ msgstr "zaslané video"

#~ msgid "sent a video note"
#~ msgstr "zaslaná video poznámka"

#~ msgid "sent a voice note"
#~ msgstr "zaslaná hlasová správa"

#~ msgid "joined by invite link"
#~ msgstr "sa pripojil pomocou pozvánky"

#~ msgid "Message"
#~ msgstr "Správa"
